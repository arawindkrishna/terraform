# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/vmware/vra" {
  version = "0.4.1"
  hashes = [
    "h1:T5RVkGwk2RLz+K7vxYnWYtKOGZ+hYLl/9Kj2f3P2EaU=",
    "zh:00913fa4b7dfac8ece07227ee83ce37aee8be963c94774c0953f66ef182c9158",
    "zh:0fa58e3411c0c5ba1cd74031870bd0883fab479c26479985fde31240718ea9ec",
    "zh:1597788412ea4e1e3cf7dc2e5aecb3bd493946e3e68977e48efa5f728c5e19fe",
    "zh:19be88019112e1dadee11926da4f691b88a82f310206a8973a3ef4265dc95edb",
    "zh:24bff34d0bcf7d05d9ef795d45d9b723c93e74e5829aae7d8c98c3e415b2b28d",
    "zh:302579103c5734da69a793920e04d3eb749e1a846f0ab0c5e531dd04995662eb",
    "zh:53e5cc60f05fdc57da907dca8be79f3d61a467bc485f4d8d802e86a277a84441",
    "zh:5569f3c1aa72f33030c983f7f68f5885e72eb513d973f050e831ebb374f85f2f",
    "zh:6f6911c2bbad7968422205a005a600d8eda1995a59a8b21a06be360900ceef8f",
    "zh:83ba4c07ea0992ed41a0e42c852caa42110f04932a5f94799c788e287382fa72",
    "zh:9a3cd22939f049b69883f95cce2ab50724d00ac30deaff894a848cca1bedc859",
    "zh:a8e05cf853f35a4bffd0d892b46fb2adcb8d79ef3cff0d04c3d553c0651b3a17",
    "zh:e1f887742e675b8b3313522e3509231e6e30ea6f4da7ee1e0b45c264c24d6f73",
    "zh:e874d49da4a9e8c95063ad9250ab87386ab23be460705dc663f037e995bcc8ce",
  ]
}
