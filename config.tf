terraform {
  required_providers {
    vra = {
      source  = "vmware/vra"
    }
  }
  required_version = ">= 0.13"
}

provider "vra" {
  url           = var.vra_url
  refresh_token = var.vra_refresh_token
  insecure      = false
}

resource "vra_deployment" "demo" {
  name        = var.deployment_name
  description = "Deployment description"

  catalog_item_id      = "c588f6a0-6efe-3e0e-954a-f78890f53c86"
  catalog_item_version = "1.0"
  project_id           = "e06ed66d-3629-4f49-a0af-834e9bd671cd"

  inputs = {
    cpu = 2
    memory = 4
    hostname = "indrtest003"
  }

  timeouts {
    create = "30m"
    delete = "30m"
    update = "30m"
  }
}